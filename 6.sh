#!/usr/bin/env bash

#variables globales
cd $(pwd)
if [ -d temporales ]; 
then
		rm -r temporales
		mkdir -p temporales
	else
		mkdir -p temporales

fi

cd temporales

Fecha=$(date +%d%m%y)
Periodico="Financiero"



# wget --header 'Host: www.elfinanciero.com.mx' --user-agent 'Mozilla/5.0 (X11; Linux x86_64; rv:71.0) Gecko/20100101 Firefox/71.0' --header 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' --header 'Accept-Language: es-AR,es-MX;q=0.5' --referer 'https://elfinanciero.com.mx/graficos/folds/fold-portada-digital.html' --header 'Cookie: ac_enable_tracking=1' --header 'Upgrade-Insecure-Requests: 1' 'https://www.elfinanciero.com.mx/graficos/pdf/download.php?file=ef-portada-digital.pdf' --output-document 'ef-portada-digital.pdf'


wget --header 'Host: www.elfinanciero.com.mx' --user-agent 'Mozilla/5.0 (X11; Linux x86_64; rv:71.0) Gecko/20100101 Firefox/71.0' --header 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' --header 'Accept-Language: es-AR,es-MX;q=0.5' --referer 'https://elfinanciero.com.mx/graficos/folds/fold-portada-digital.html' --header 'Cookie: ac_enable_tracking=1' --header 'Upgrade-Insecure-Requests: 1' "https://www.elfinanciero.com.mx/graficos/suplementos/pdf/ef$Fecha.pdf" --output-document "ef-$Fecha.pdf"


# pdftk "ef-portada-digital.pdf" cat 1 output preview.pdf
# convert preview.pdf preview.png

## Pagina Final (contraportada)
# Comando para saber cuantas paginas tiene el pdf

# paginaFinal=$(pdftk JornadaHoy.pdf  dump_data | grep NumberOfPages |  awk '{print $2}')

# pdftk JornadaHoy.pdf cat $paginaFinal output z.pdf
# convert z.pdf z.png

## Unir portada y contraportada
# convert +append preview.png z.png az.png


# Subimos el PDF via Telegram

# # Refrescamos lista de canales
telegram-cli -C -W -e channel_list

# # Mensaje
#formato texto gracias a # https://beautifuldingbats.com/hey-howd-you-do-that
telegram-cli -W -e  "msg JAROCHOS "$Periodico" "

# # Mandamos vista previa de porta y contra portada
# telegram-cli -W -e  "send_photo JAROCHOS preview.png"

# # Mandamos periódico en pdf
telegram-cli -W -e  "send_document JAROCHOS ef-$Fecha.pdf"
