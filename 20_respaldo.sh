#!/usr/bin/env bash

if [ -f 101.txt ];
then
rm 101.txt
# echo "Si, si existe"
else
echo "No, no existe archivos previos"
fi

# Descargamos informacion de hojas
wget -U Mozilla/5.0 --post-data  'UID=PNIDGL3yVq672pd'  https://servicios.epaper.milenio.com/api/v1/epaper/publication/101
mv 101 101.txt

# Del archivo  101 extraemos la información

# cat 101.txt | awk -F ',' '{print $12}' | awk -F ':' '{print $2}' | sed 1d 

paginaFinal=$(cat 101.txt | awk -F ',' '{print $12}' | awk -F ':' '{print $2}' | sed 1d )
fechaActual=$(date  '+%Y%m%d')
cat 101.txt | awk -F ',' '{print $7}' | awk -F ':' '{print $2}' | sed 1d
echo "son $paginaFinal paginas a descargar"

 for (( i = 0; i < "$paginaFinal" ; i++ )); do
 	wget --limit-rate=100k --wait=5  -U Mozilla/5.0 --post-data  'UID=PNIDGL3yVq672pd' https://servicios.epaper.milenio.com/api/v1/epaper/mil/Nacional/NAC/$fechaActual/images/$i.jpg

 done

#el siguiente comando para convertir en pdf las imagenes en orden fue para mi un calvario xD
#pero la solución estaba como siempre en stackoverflow xD
# https://stackoverflow.com/questions/44215252/img2pdf-how-to-give-file-range#44215295
img2pdf --output Milenio.pdf $(ls *.jpg|sort -n)

stj  Milenio.pdf
rm 101.txt metaPreview.pdf Milenio.pdf
