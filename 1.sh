#!/bin/bash

cd $(pwd)
mkdir -p temporales
cd temporales

## Borramos posibles archivos temporales de la descarga anterior

# Borrramos  archivos index
if [ -f index.html ];
then
rm index.html
# echo "Si, si existe"
else
echo "No, no existe"
fi
# Borramos url del pdf
if [ -f urlpdf ];
then
rm urlpdf
# echo "Si, si existe"
else
echo "No, no existe"
fi



#es necesario borrar  archivos anterirores
if [ -f a.pdf ];
then
rm *.pdf
# echo "Si, si existe"
else
echo "No, no existe"
fi
if [ -f a.png ];
then
rm *.png
# echo "Si, si existe"
else
echo "No, no existe"
fi




## Bajamos el index para extraer buscar el pdf
wget --user-agent="Mozilla/5.0 (iPhone; CPU OS 10_14 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.1 Mobile/14E304 Safari/605.1.15" "https://digital.jornada.com.mx/ciudad/Nacional/"

## Buscamos el pdf y lo exponemos en el un archivo de texto plano llamado urlpdf
################################################################################################################################
# gracias por el codigo https://unix.stackexchange.com/questions/56675/how-can-i-use-awk-to-extract-urls-from-a-html-file#56677
################################################################################################################################

sed -ne 's/.*\(http[^"]*\).*/\1/p' < index.html | grep pdf > urlpdf

# Descargamos el pdf y lo guardamos como JornadaHoy.pdf
wget --user-agent="Mozilla/5.0 (iPhone; CPU OS 10_14 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.1 Mobile/14E304 Safari/605.1.15"  -i urlpdf -O JornadaHoy.pdf






### Generamos vista previa del pdf
## Pagina Inicial (portada)

pdftk JornadaHoy.pdf cat 1 output a.pdf
convert a.pdf a.png

## Pagina Final (contraportada)
# Comando para saber cuantas paginas tiene el pdf

paginaFinal=$(pdftk JornadaHoy.pdf  dump_data | grep NumberOfPages |  awk '{print $2}')

pdftk JornadaHoy.pdf cat $paginaFinal output z.pdf
convert z.pdf z.png

## Unir portada y contraportada
convert +append a.png z.png az.png


# Subimos el PDF via Telegram

# # Refrescamos lista de canales
#telegram-cli -C -W -e channel_list

# # Mensaje
#formato texto gracias a # https://beautifuldingbats.com/hey-howd-you-do-that
#telegram-cli -W -e  "msg JAROCHOS 𝕷𝖆 𝕵𝖔𝖗𝖓𝖆𝖉𝖆 "

# # Mandamos vista previa de porta y contra portada
#telegram-cli -W -e  "send_photo JAROCHOS az.png"
stj az.png
stj JornadaHoy.pdf
# # Mandamos periódico en pdf
#telegram-cli -W -e  "send_document JAROCHOS JornadaHoy.pdf"
